extends Item

# 更新属性物品
class_name PropertyItem

# 更新用户属性名称
@export var player_property_name: String;

# 更新用户属性内容
@export var player_property_value: int = 1;

## 玩家触碰
func _on_player_collide(player: Player):
	if player_property_name == "":
		return false
	if player[player_property_name] + player_property_value < 0:
		return false
	player[player_property_name] += player_property_value
	super._on_player_collide(player)
	if sfx != null:
		self._audio.play_sfx(sfx)
	return true
