extends Node2D

# 交互物品
class_name Item

## 使用后播放的音效
@export var sfx : AudioStream

## 音频
@onready var _audio: Audio = $"/root/LevelManager/Audio"

# 玩家触碰
func _on_player_collide(player: Player):
	if self._is_use_free():
		self.queue_free()
	return true

## 是否调用free
func _is_use_free():
	return true
