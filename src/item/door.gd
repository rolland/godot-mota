extends PropertyItem

## 门
class_name Door

@onready var _sprite: Sprite2D = $"Sprite2D"

@onready var _static_body: StaticBody2D = $"StaticBody2D"

## 是否被使用
var _used = false

## 打开门
func open():
	var sprite_region = _sprite.region_rect
	var tween = create_tween()
	tween.parallel().tween_property(self._sprite, "region_rect", Rect2(sprite_region.position.x \
		,sprite_region.position.y, sprite_region.size.x, 0),0.25)
	tween.parallel().tween_property(self._sprite, "position", 
		self._sprite.position + Vector2(0,8),0.25)
	tween.tween_callback(self.queue_free)

## 玩家触碰
func _on_player_collide(player: Player):
	if _used:
		return false
	var result = super._on_player_collide(player)
	if result:
		_used = true
		self.open()
	return false

## 不立即调用free，修改为动画后调用
func _is_use_free():
	return false
