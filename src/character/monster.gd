extends Character

## 怪物类
class_name Monster

func _ready():
	add_to_group("monster")

## 玩家触碰开始战斗
func _on_player_collide(player: Player):
	player.set_process_input(false)
	var cf = CharacterFight.new(player, self)
	var cf_result = await cf.fight()
	player.set_process_input(true)
	if cf_result == -1:
		hud.show_toast("%s无法打败%s" % [player.character_name, self.character_name])
		return false
	if cf_result == 1:
		hud.show_toast("%s打败了%s" % [player.character_name, self.character_name])
		return false
	if cf_result == 0:
		hud.show_toast("%s打败了%s" % [self.character_name, player.character_name])
		return false
	return false

## 怪物死亡
func _on_dead():
	self.queue_free()
