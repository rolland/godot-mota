extends NPC

## 商店
class_name Shop

## 第一次购买需要的金币
@export var init_money : int = 20
## 第一次购买增加的血量
@export var init_hp : int = 100
## 购买增加攻击力
@export var added_atk : int = 2
## 购买增加的防御力
@export var added_def : int = 4

## 使用次数
var _count: int = 1

## 获取购买需要的金币
func get_require_money() -> int:
	return _count * init_money

## 获取增加的血量
func get_added_hp() -> int:
	return _count * init_hp

## 检查是否有足够的金币
func check_require_money(player: Player) -> bool:
	if player.money >= self.get_require_money():
		return true
	hud.show_toast("你没有足够的金币")
	return false

## 读取数据
func get_data() -> Dictionary:
	return {
		"_count": _count
	}

## 设置数据
func set_data(data: Dictionary):
	self._count = data["_count"]

## 获取npc对话
func _get_dialogue() -> String:
	return "如果供奉%d金币，便可以增加你的力量，你想要什么呢..." % [self.get_require_money()]

## 获取npc可以执行的行为
func _get_actions() -> Array:
	return [
	{
		"text": "生命+%d" % [self.get_added_hp()],
		"pressed": self._on_action1.bind(),
	},
	{
		"text": "攻击+%d" % [self.added_atk],
		"pressed": self._on_action2.bind(),
	},
	{
		"text": "防御+%d" % [self.added_def],
		"pressed": self._on_action3.bind(),
	},
	{
		"text": "离开",
		"pressed": _on_action4.bind()
	}
	]

## 点击增加血量的逻辑
func _on_action1():
	var player = $"/root/LevelManager/Player" as Player
	if not self.check_require_money(player):
		return
	player.money -= self.get_require_money()
	player.hp += self.get_added_hp()
	self._count += 1
	hud.hidde_npc_dialog()

## 点击增加攻击力的逻辑
func _on_action2():
	var player = $"/root/LevelManager/Player" as Player
	if not self.check_require_money(player):
		return
	player.money -= self.get_require_money()
	player.atk += self.added_atk
	self._count += 1
	hud.hidde_npc_dialog()
	
## 点击增加防御力的逻辑
func _on_action3():
	var player = $"/root/LevelManager/Player" as Player
	if not self.check_require_money(player):
		return
	player.money -= self.get_require_money()
	player.def += self.added_def
	self._count += 1
	hud.hidde_npc_dialog()

## 点击离开的逻辑
func _on_action4():
	hud.hidde_npc_dialog()
