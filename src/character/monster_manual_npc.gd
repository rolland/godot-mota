extends NPC

## 赠送怪物手册的NPC
class_name MonsterManualNPC

## 获取npc对话
func _get_dialogue() -> String:
	return "我给你怪物手册，它能预测当前楼层各类怪物对你的伤害"

## 获取npc可以执行的行为
func _get_actions() -> Array:
	return [{
		"text": "收下",
		"pressed": _on_action1.bind()
	}]

## 点击收下后的逻辑
func _on_action1():
	var player = $"/root/LevelManager/Player" as Player
	player.add_prop(Constant.MONSTER_MANUAL)
	hud.hidde_npc_dialog()
	var tween = create_tween()
	tween.tween_property(self, "modulate", Color(1.0,1.0,1.0,0.0), 0.5)
	tween.tween_callback(self.queue_free)
