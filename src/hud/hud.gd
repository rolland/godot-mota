extends CanvasLayer

## 用户界面
class_name HUD

@onready var _toast_container : NinePatchRect = $"ToastContainer"

@onready var _toast_label : Label = $"ToastContainer/ToastLabel"

## 显示提示
func show_toast(message: String):
	_toast_label.text = message
	_toast_label.visible_ratio = 0.0
	# 显示出现动画
	var t1 = create_tween()
	t1.tween_property(_toast_container, "modulate",Color(1.0,1.0,1.0,1.0), 0.25)
	t1.tween_property(_toast_label, "visible_ratio", 1.0, 0.5)
	await t1.finished;
	# 显示消失动画
	await get_tree().create_timer(2.0).timeout
	var t2 = create_tween()
	t2.tween_property(_toast_container, "modulate", Color(1.0,1.0,1.0,0.0), 0.5)
	await t2.finished

## 显示npc聊天弹窗
func show_npc_dialog(_name: String, message: String, buttons: Array = [], closeable = true):
	$"NPCContainer/MarginContainer/VBoxContainer/NpcName".text = _name
	$"NPCContainer/MarginContainer/VBoxContainer/NpcDialogue".text = message
	var action_container = $"NPCContainer/MarginContainer/VBoxContainer/NpcActionContainer" as VBoxContainer
	for child in action_container.get_children():
		action_container.remove_child(child)
		child.queue_free()
	for button in buttons:
		var btn = Button.new()
		btn.text = button.text
		btn.pressed.connect(button.pressed)
		action_container.add_child(btn)
	if closeable:
		var btn = Button.new()
		btn.text = "关闭"
		btn.pressed.connect(hidde_npc_dialog)
		action_container.add_child(btn)
	$"NPCContainer".visible = true
	
func hidde_npc_dialog():
	$"NPCContainer".visible = false

func _on_player_hp_changed(hp:int):
	$"LeftContainer/HPText".text = str(hp)

func _on_player_def_changed(def):
	$"LeftContainer/DefText".text = str(def)

func _on_player_atk_changed(atk):
	$"LeftContainer/AtkText".text = str(atk)

func _on_player_money_changed(money):
	$"LeftContainer/MoneyText".text = str(money)

func _on_player_yellow_key_changed(count):
	$"RightContainer/KeyBackground/YellowKeyCount".text = "%02d" % [count]

func _on_player_blue_key_changed(count):
	$"RightContainer/KeyBackground/BlueKeyCount".text = "%02d" % [count]

func _on_player_red_key_changed(count):
	$"RightContainer/KeyBackground/RedKeyCount".text = "%02d" % [count]

func _on_level_manager_level_changed(level):
	$"LeftContainer/LevelTitle".text = "第%02d层" % [level]

func _on_player_prop_added(prop):
	if prop == Constant.MONSTER_MANUAL:
		$"PropContainer/MonsterManual".visible = true

func _on_monster_manual_pressed():
	var mmc = $"MonsterManualContainer" as NinePatchRect
	mmc.visible = !mmc.visible
	if mmc.visible:
		var ml = $"MonsterManualContainer/MonsterList" as ItemList
		var monsters = get_tree().get_nodes_in_group("monster")
		var monsters_distinct = {}
		for monster in monsters:
			monsters_distinct[monster.character_name] = monster
		ml.clear()
		for monster_name in monsters_distinct:
			var m = monsters_distinct[monster_name] as Monster
			var m_sprite = m.get_node("Sprite2D") as Sprite2D
			var item_texture = AtlasTexture.new()
			item_texture.atlas = m_sprite.texture
			item_texture.region = m_sprite.region_rect
			ml.add_item("%s 攻%d 防%d 金%d" % [m.character_name, m.atk, m.def, m.money], 
				item_texture)

func _on_save_button_button_down():
	var level_manager = get_parent() as LevelManager
	level_manager.save_data(0)
	pass

func _on_load_button_button_down():
	var level_manager = get_parent() as LevelManager
	level_manager.load_data(0)
